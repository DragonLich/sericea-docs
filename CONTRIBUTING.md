# Contributing
Please check the Fedora Sericea Docs' ["Contributing"](https://docs.fedoraproject.org/en-US/fedora-sericea/contributing/) page.
