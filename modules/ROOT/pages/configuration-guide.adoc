[[configuration-guide]]
:config-repo:  https://gitlab.com/fedora/sigs/sway/sway-config-fedora
:config-dir:   {config-repo}/-/tree/fedora
:config-file:  {config-repo}/-/blob/fedora

= Configuration Guide

This section provides a brief overview of configuration details specific for
Fedora and is common for both the classic Sway Spin and the Sericea (OSTree).
Important differences will be highlighted.

If you are looking for more general resources, please check

 * man pages:
   https://man.archlinux.org/man/sway.5[`sway(5)`],
   https://man.archlinux.org/man/sway-bar.5[`sway-bar(5)`]
   https://man.archlinux.org/man/sway-input.5[`sway-input(5)`]
   https://man.archlinux.org/man/sway-output.5[`sway-output(5)`]
 * https://github.com/swaywm/sway/wiki[Official Sway wiki]
 * https://i3wm.org/docs/userguide.html[i3 User's Guide].
   While it's written for i3, all the important concepts are the same for Sway.

== Configuration profiles

The Sway package in Fedora defers most of the dependencies and the config file
ownership to the `+sway-config-*+` subpackages.
This allows us to ship different configuration profiles with different sets of
runtime dependencies.

The following profile packages are currently available in Fedora:

 * **sway-config-upstream** - the upstream configuration as it comes with the
   Sway sources.
   The only permitted modifications to this profile's config file are
   adjustments for dependencies currently unavailable in Fedora.

 * **sway-config-minimal** - minimal configuration with any optional
   dependencies excluded.
   The profile was created for headless servers, containers and buildroot
   usage.
   It is also suitable for building a very minimal installation from scratch.

 * **sway-config-fedora** - customized configuration for Fedora Sway Spin.

The packages are mutually exclusive, and one of these must be present.
The one installed by default with `+dnf install sway+` is **sway-config-upstream**
and the one that will be installed with Sway Spin/Sway Desktop Environment group
is **sway-config-fedora**.

You can select the profile with following command:

[source,shell]
----
dnf swap sway-config sway-config-upstream
dnf swap sway-config sway-config-minimal
dnf swap sway-config sway-config-fedora
----

The command will replace the default `+/etc/sway/config+` file and apply the new
set of dependencies.
Packages unused by the new profile will be autoremoved.

The corresponding incantation for Sericea and other custom Sway OSTree images is

[source,shell]
----
rpm-ostree override remove sway-config --install sway-config-upstream
----

Note, however, that it does not remove the original profile dependencies from
the base layer of a deployment.

== Fedora configuration

link:{config-repo}[**sway-config-fedora**] contains the default configuration
for Fedora Sway Spin.
It is built on top of the default Sway config with a few quality of life
improvements and opinionated changes.

Most of the additions are implemented as a standalone configuration snippets
stored at link:{config-dir}/sway/config.d[`+/usr/share/sway/config.d/+`]
and automatically loaded from the main configuration file.
Environment configuration and logging interception are implemented in the
link:{config-file}/sway/start-sway[`+/usr/bin/start-sway+`] wrapper script.

=== Environment variables

If you are using a compatible Display Manager (GDM or SDDM), you should be able
to adjust the initial environment for Sway using a system-wide configuration
at `+/etc/sway/environment+` or per-user config at `+~/.config/sway/environment+`.
The variables set in the user configuration overwrite matching variables
set system-wide.

Please, check the comments and examples in
link:{config-file}/sway/environment[`+/etc/sway/environment+`] to learn more.

=== Sections

The configuration snippets we provide are grouped into the following sections:

50-59 (`+50-rules-*.conf+`)::
Window rules (`+for_window+`, `+assign+` and related configuration).

60-69 (`+60-bindings-*.conf+`, `+65-mode-*.conf+`)::
Key bindings and binding modes

90-94 (`+90-*.conf+`)::
System applications: bars, idle daemons and other components.

95-99 (`+95-*.conf+`)::
Autostart applications

=== Overrides and load precedence

Fedora configuration +++<del>ab</del>+++uses the implementation details of the
Sway configuration parser
(https://man7.org/linux/man-pages/man3/wordexp.3p.html[`wordexp(3p)`]) to
implement an overrides mechanism for the snippets.

The priority increases from the packaged configuration to a system-wide
configuration and an user configuration directories:

 * `+/usr/share/sway/config.d/*.conf+`
 * `+/etc/sway/config.d/*.conf+`
 * `+$XDG_CONFIG_HOME/sway/config.d/*.conf+`
    (defaults to `+~/.config/sway/config.d/*.conf+`)

The includes are also sorted by a file name across all the directories.

By creating a file with the same name in `+/etc/sway/config.d+` you'll force
the config preprocessor to ignore the corresponding snippet from `+/usr+` and
load the one from `+/etc+`.
Similarly, the configuration snippet from a home directory wins over the earlier
locations.

To put it even more simple: imagine the distribution configuration file
`+/usr/share/sway/config.d/90-bar.conf+` that sets `waybar` as a status bar.
If you want to prevent `waybar` from starting, you could create an empty file
in your home directory:

[source,shell]
----
mkdir -p ~/.config/sway/config.d
touch ~/.config/sway/config.d/90-bar.conf
----

If you want to use another bar, you just need to add some contents to the file.
For example, copy the default bar section from the upstream Sway config:

[source,shell]
----
mkdir -p ~/.config/sway/config.d
cat >~/.config/sway/config.d/90-bar.conf <<EOF
# Read `man 5 sway-bar` for more information about this section.
bar {
    position top

    # When the status_command prints a new line to stdout, swaybar updates.
    # The default just shows the current date and time.
    status_command while date +'%Y-%m-%d %I:%M:%S %p'; do sleep 1; done

    colors {
        statusline #ffffff
        background #323232
        inactive_workspace #32323200 #32323200 #5c5c5c
    }
}
EOF
----

=== Troubleshooting

Sometimes it's useful to know how the configuration loaded by Sway actually looks.
There are two ways to debug that:

 * Run sway config validation:
[source,shell]
----
sway --debug --validate [--config /path/to/config]
----

 * Check intermediate files generated by the layered include script

[source,shell]
----
less $XDG_RUNTIME_DIR/sway/layered-include-*.conf
----
